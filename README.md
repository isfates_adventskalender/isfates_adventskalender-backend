# ISFATES_Adventskalender-backend

----
### This project use:

* PHP :
    * [PHPMailer](https://github.com/PHPMailer/PHPMailer) to send email with PHP
    * [Monolog](https://github.com/Seldaek/monolog/blob/master/README.md) to manage logs
    * [Composer](https://getcomposer.org/) to manage PHP's package
    * [Behat **v2.5**](http://docs.behat.org/en/v2.5/) to test the API and the app
    

* Python3 :
    * [Google Drive API](https://developers.google.com/sheets/api/quickstart/python) to get data from Drive


----
### Installation:

1. Install [Composer](https://getcomposer.org/)

2. Edit all `api-settings.json` with yours settings (there is one in `/api` and one in `/admin`)
    
    * `api-settings.json` structure in `/api`:
    ```json
    {
        "name": "ISFATES-adventskalender-backend",
        "version": "<version>",
        "devs_email": ["<emails>"],
        "path_to_images": "<path/to/imgs/>",
        "api_to_use": "<local || server>",
        "api": {
            "local": "<link/to/api/>",
            "server": "<link/to/api/>"
        },
        "gmail": {
            "gmail_api_credentials": "/path/to/file"
        },
        "crypto": {
            "key_to_encrypt_path": "/path/to/file"
        },
        "database": {
            "db_credentials_path": "/path/to/file"
        },
        "logs_handler": {
            "logs_folder_path": "/path/to/file"
        }
    }
    ```
    
    * `api-settings.json` structure in `/admin`:
    ```json
    {
        "name": "ISFATES-adventskalender-backend-admin",
        "version": "<version>",
        "path_to_python": "/path/to/python",
        "drive_api": {
            "credentials_path": "/path/to/file"
        },
         "gmail": {
            "gmail_api_credentials": "/path/to/file"
        },
        "crypto": {
            "key_to_encrypt_path": "/path/to/file"
        },
        "database": {
            "db_credentials_path": "/path/to/file"
        },
        "logs_handler": {
            "logs_folder_path": "/path/to/file"
        }
    }
    ```
    
    ##### ! You have to get all this files from an admin
    
3. Import database structure `/src/database.sql` to your local database

4. Be sure that python run on typing `python -V` (It have to return smthg like `Python 3.7.<?>`)

5. Make `pip install` for:
    * `gspread`
    * `oauth2client`

6. To run testing on local you will probably have to set the `curl.cainfo` in your `php.ini`. Check [here](https://stackoverflow.com/questions/29822686/curl-error-60-ssl-certificate-unable-to-get-local-issuer-certificate) for more infos