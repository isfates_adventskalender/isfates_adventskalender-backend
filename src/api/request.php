<?php

// IMPORT CRYPTO LIBRARY ZONE
require_once dirname(__FILE__) . "/../vendor/autoload.php";
require_once dirname(__FILE__) . "/myCrypto.php";
// END IMPORT CRYPTO LIBRARY ZONE

// IMPORT LOGS HANDLER ZONE
require_once dirname(__FILE__) . "/logsHandler.php";
// END IMPORT LOGS HANDLER ZONE

require_once dirname(__FILE__) . "/apiSettings.php";

require_once dirname(__FILE__) . "/databaseHandler.php";
require_once dirname(__FILE__) . "/emailSender.php";


require_once dirname(__FILE__) . "/pathToDefaultsClasses.php";


interface IRequest
{
    public function request();
}


class Request
{
    protected $apiSettings;

    protected $userHeader;
    protected $userBody;

    protected $logsHandler;
    protected $myCrypto;
    protected $adventsKalenderDB;
    protected $emailSender;

    public function __construct()
    {
        $this->apiSettings = new ApiSettings();

        $this->myCrypto = new MyCrypto($this->apiSettings->getCryptoSettings());

        $this->emailSender = new EmailSender($this->apiSettings->getGmailSettings(), function (string $errorMsg) {
            $this->onSendEmailError($errorMsg);
        });

        $logsHandlerSettings = array_merge($this->apiSettings->getLogsHandlerSettings(), array("devs_email" => $this->apiSettings->getDevsEmail()));
        $this->logsHandler = new LogsHandler($logsHandlerSettings, $this->emailSender);

        $this->adventsKalenderDB = new DatabaseHandler(($this->apiSettings->getDatabaseSettings())["db_credentials_path"], function (string $errorMsg) {
            $this->onUnavailableDatabase($errorMsg);
        });
    }

    protected function getSpecificApiSettings(string $settings)
    {
        return $this->API_SETTINGS[$settings];
    }

    protected function getRequestData()
    {
        if ($this->checkRequestExistence()) {
            $this->userHeader = $_POST["header"];
            $this->userBody = $_POST["request"];
        }
    }

    public function checkRequestExistence()
    {
        if (!isset($_POST["header"]["private_key"]) || !$this->isPrivateKeyValid($_POST["header"]["private_key"])) {
            $this->returnErrorResponse("Invalid private key !");
            $this->addLog("warning", "Invalid private key !");
        }

        if (!isset($_POST["request"])) {
            $this->returnErrorResponse("Request have to contain a request !");
            $this->addLog("warning", "Invalid request with invalid request");
        }

        if (!isset($_POST["header"])) {
            $this->returnErrorResponse("Request have to contain a header !");
            $this->addLog("warning", "Invalid request with invalid header");
        }

        return TRUE;
    }

    protected function isPrivateKeyValid(string $privateKey)
    {
        return $privateKey === "96436d48-79150c9f-reb3720a-1d352ea9"; // TODO: Don"t save it in code !
    }

    public function returnErrorResponse(String $msg)
    {
        $this->returnJson(
            array(
                "success" => FALSE,
                "msg" => $msg,
            )
        );

        die();
    }

    public function returnSuccessResponse(array $response)
    {
        $this->returnJson(
            array(
                "success" => TRUE,
                "response" => $response,
            )
        );

        die();
    }

    protected function returnJson(array $array)
    {
        echo (json_encode($array));
    }

    public function getDecryptedToken(string $cryptedToken)
    {
        return $this->myCrypto->decrypt($cryptedToken);
    }

    protected function addLog(string $logType, string $title, array $additionalData = [])
    {
        $this->logsHandler->addLog($logType, $title, $additionalData);
    }


    // ###### ERROR HANDLING ZONE
    protected function onUnavailableDatabase(string $errorMsg)
    {
        $this->addLog("alert", "Unavailable Database", array(
            "error" => $errorMsg,
        ));
    }

    protected function onSendEmailError(string $errorMsg)
    {
        $this->addLog("debug", "Error on send email", array(
            "error" => $errorMsg,
        ));
    }
    // ###### END ERROR HANDLING ZONE


    // ###### SPECIFIC METHODS ZONE
    protected function isTokenValid(string $cryptedToken)
    {
        $token = $this->getDecryptedToken($cryptedToken);

        if ($this->adventsKalenderDB->isPresent("SELECT * FROM token WHERE id= :id", array(
            "id" => $token,
        ))) {
            return TRUE;
        }

        return FALSE;
    }

    public function getUserId(string $cryptedToken): string
    {
        $token = $this->getDecryptedToken($cryptedToken);

        $getUserIdResponse = $this->adventsKalenderDB->get("SELECT user_id FROM token WHERE id= :id", array(
            "id" => $token,
        ));

        return $getUserIdResponse[0]["user_id"];
    }
    // ###### END SPECIFIC METHODS ZONE

}
