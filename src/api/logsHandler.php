<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require_once dirname(__FILE__) . "/emailSender.php";

class LogsHandler
{
    protected $logsHandler;
    protected $logsHandlerSettings;
    protected $emailSender;

    public function __construct(array $logsHandlerSettings, $emailSender)
    {
        $this->logsHandlerSettings = $logsHandlerSettings;
        $this->emailSender = $emailSender;
    }

    protected function initLogsHandler()
    {
        $this->logsHandler = new Logger("logger");
        $this->logsHandler->pushHandler(new StreamHandler($this->getLogFilePath(), Logger::DEBUG));
    }

    protected function getLogFilePath()
    {
        return $this->logsHandlerSettings["logs_folder_path"] . date("Y-m-d") . ".log";
    }

    public function addLog(string $logType, string $title, array $additionalData = [])
    {
        $this->initLogsHandler();

        switch ($logType) {
            case "debug": // Detailed debug information.
                $this->logsHandler->debug($title, $additionalData);
                break;

            case "info": // Interesting events. Examples: User logs in, SQL logs.
                $this->logsHandler->info($title, $additionalData);
                break;

            case "notice": // Normal but significant events.
                $this->logsHandler->notice($title, $additionalData);
                break;

            case "warning": // Exceptional occurrences that are not errors
                $this->logsHandler->warning($title, $additionalData);
                break;

            case "error": // Runtime errors that do not require immediate action but should typically be logged and monitored.
                $this->logsHandler->error($title, $additionalData);
                break;

            case "critical": // Critical conditions. Example: Application component unavailable, unexpected exception.
                $this->logsHandler->critical($title, $additionalData);
                $this->sendAlertMail("critical", $title, $additionalData);
                break;

            case "alert": // Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
                $this->logsHandler->alert($title, $additionalData);
                $this->sendAlertMail("alert", $title, $additionalData);
                break;

            case "emergency": // Emergency: system is unusable.
                $this->logsHandler->emergency($title, $additionalData);
                $this->sendAlertMail("emergency", $title, $additionalData);
                break;
        }
    }

    protected function sendAlertMail(string $type, string $title, array $additionalData)
    {
        $errorOverview = substr($additionalData["error"], 0, 20); // Get first 20 char from error string

        $emailConstructor = array(
            "email"           => $this->logsHandlerSettings["devs_email"],
            "subject"         => "ISFATES-AK : {$type}-log // " . $errorOverview . "...",
            "bodyConstructor" => array(
                "pathToTemplate" => dirname(__FILE__) . "/alertEmailTemplate.html",
                "replacer"       => array(
                    "__TYPE__"            => $type,
                    "__TITLE__"           => $title,
                    "__ADDITIONAL_DATA__" => json_encode($additionalData, JSON_PRETTY_PRINT),
                ),
            )
        );

        $this->emailSender->sendEmail($emailConstructor);
    }
}
