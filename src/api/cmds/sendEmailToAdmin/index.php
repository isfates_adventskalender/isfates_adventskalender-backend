<?php

require_once "../../request.php";


class SendEmailToAdmin extends Request implements IRequest
{
    protected $cmdName = "SendEmailToAdmin";

    public function request()
    {

        try {
            $this->getRequestData();

            $this->adventsKalenderDB->connect();

            if ($this->isTokenValid($this->userBody["token"])) {

                $userId = $this->getUserId($this->userBody["token"]);

                if ($this->isUserSpamming($userId)) {
                    $this->returnSuccessResponse(
                        array(
                            "hasEmailBeSend" => FALSE,
                            "message" => "Please wait a little bit before sending a new message !",
                        )
                    );
                } else {
                    $this->sendEmail($userId);
                }
            } else {
                $this->addLog("notice", "Try to connect with invalid token", array(
                    "cmd" => $this->cmdName,
                ));

                $this->returnErrorResponse("Invalid token");
            }


            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function sendEmail(string $userId)
    {
        $userEmail = $this->getUserEmail($userId);
        $emailConstructor = array(
            "senderEmail"     => $userEmail,
            "email"           => array("adventskalender.isfates@gmail.com"),
            "subject"         => $userEmail . " - " . $this->userBody["subject"],
            "bodyConstructor" => array(
                "pathToTemplate" => "./sendEmailToAdminTemplate.html",
                "replacer"       => array(
                    "__USER_EMAIL__" => $userEmail,
                    "__SUBJECT__"    => $this->userBody["subject"],
                    "__MESSAGE__"    => $this->userBody["msg"],
                ),
            )
        );

        $this->emailSender->sendEmail($emailConstructor);

        if ($this->emailSender->hasEmailBeenSend) {

            $this->updateUserLastmsgSendToAdminDate($userId);

            $this->addLog("info", "User send mail to ADMIN", array(
                "cmd"    => $this->cmdName,
                "userId" => $this->getUserId($this->userBody["token"])
            ));

            $this->returnSuccessResponse(
                array(
                    "hasEmailBeSend" => TRUE,
                )
            );
        } else {
            $this->returnErrorResponse("Impossible to send Email !");
        }
    }

    protected function isUserSpamming(string $userId): bool
    {
        $MIN_MS_TO_RESEND_MAIL = 600; // 10min in ms
        $timestamp = Helper::getTimestamp();
        $userLastmsgSendToAdminDate = (int) $this->getUserLastmsgSendToAdminDate($userId);

        // If user has never send email to admin
        if ($userLastmsgSendToAdminDate === 0) {
            return FALSE;
        }

        if ($timestamp - $userLastmsgSendToAdminDate <= $MIN_MS_TO_RESEND_MAIL) {
            return TRUE;
        }

        return FALSE;
    }

    protected function getUserLastmsgSendToAdminDate(string $userId)
    {
        $userLastmsgSendToAdminDateResponse = $this->adventsKalenderDB->get("SELECT last_msg_send_to_admin_date FROM user WHERE id= :id", array(
            "id" => $userId,
        ));

        return $userLastmsgSendToAdminDateResponse[0]["last_msg_send_to_admin_date"];
    }

    protected function getUserEmail(string $userId)
    {
        $userEmailResponse = $this->adventsKalenderDB->get("SELECT email FROM user WHERE id= :id", array(
            "id" => $userId,
        ));

        return $userEmailResponse[0]["email"];
    }

    protected function updateUserLastmsgSendToAdminDate(string $userId)
    {
        $this->adventsKalenderDB->query(
            "UPDATE user SET last_msg_send_to_admin_date = :newDate WHERE id = :userId",
            array(
                "newDate" => (string) Helper::getTimestamp(),
                "userId" => $userId,
            )
        );
    }
}

$sendEmailToAdmin = new SendEmailToAdmin();
$sendEmailToAdmin->request();
