<?php

require_once "../../request.php";

class SendForgetPwdMail extends Request implements IRequest
{
    protected $cmdName = "SendForgetPwdMail";

    public function request()
    {

        $this->getRequestData();

        try {
            $this->adventsKalenderDB->connect();

            $doesEmailExist = $this->doesEmailExistInDB($this->userBody["email"]);

            if ($doesEmailExist) {
                $this->sendForgetPwdMailRequest($this->userBody["email"]);
            } else {
                $this->returnSuccessResponse(
                    array(
                        "hasEmailBeSend" => FALSE,
                        "error_type"     => 1, // INVALID EMAIL
                    )
                );
            }

            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function sendForgetPwdMailRequest(string $email)
    {
        $resetPwdToken = Helper::getId();
        $userData = $this->getUserDataFromEmail($email);

        $userId = $userData["id"];
        $userFirstName = $userData["firstName"];

        if (!$this->doesUserIdAlreadyExist($userId)) {
            $emailConstructor = array(
                "email"           => array($email),
                "subject"         => "Reset my password : ISFATES Adventskalender",
                "bodyConstructor" => array(
                    "pathToTemplate" => "./forgetPwdMailTemplate.html",
                    "replacer"       => array(
                        "__USER_FIRST_NAME__" => $userFirstName,
                        "__PATH_TO_API__"     => $this->apiSettings->getPathToApi(),
                        "__RESET_PWD_TOKEN__" => urlencode($resetPwdToken),
                    ),
                )
            );

            $this->emailSender->sendEmail($emailConstructor);

            if ($this->emailSender->hasEmailBeenSend) {
                $this->insertResetPwdTokenInDb($resetPwdToken, $userId);

                $this->returnSuccessResponse(
                    array(
                        "hasEmailBeSend" => TRUE,
                    )
                );
            } else {
                $this->returnErrorResponse("Impossible to send Email !");
            }
        } else {
            $this->returnSuccessResponse(
                array(
                    "hasEmailBeSend" => FALSE,
                    "error_type"     => 2, // EMAIL ALLREADY SEND
                )
            );
        }
    }

    protected function insertResetPwdTokenInDb(string $resetPwdToken, string $userId)
    {
        $this->adventsKalenderDB->query(
            "INSERT INTO reset_pwd_token(resetPwdToken, userId) VALUES(:resetPwdToken, :userId)",
            array(
                "resetPwdToken" => $resetPwdToken,
                "userId"        => $userId,
            )
        );
    }

    protected function doesEmailExistInDB(string $email)
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM user WHERE email= :email", array(
            "email" => $email,
        ));
    }

    protected function doesUserIdAlreadyExist(string $userId)
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM reset_pwd_token WHERE userId= :userId", array(
            "userId" => $userId,
        ));
    }

    protected function getUserDataFromEmail(string $email): array
    {
        $getUserIdResponse = $this->adventsKalenderDB->get("SELECT id, first_name FROM user WHERE email= :email", array(
            "email" => $email,
        ));

        return array(
            "id"         => $getUserIdResponse[0]["id"],
            "firstName" => $getUserIdResponse[0]["first_name"],
        );
    }
}

$sendForgetPwdMail = new SendForgetPwdMail();
$sendForgetPwdMail->request();
