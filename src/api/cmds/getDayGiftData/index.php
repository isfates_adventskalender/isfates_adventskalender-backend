<?php

require_once "../../request.php";

class GetDayGiftData extends Request implements IRequest
{
    protected $cmdName = "GetDayGiftData";
    protected $userId = null;

    public function request()
    {

        try {
            $this->getRequestData();

            $this->adventsKalenderDB->connect();

            if ($this->isTokenValid($this->userBody["token"])) {
                $cryptedToken = $this->userBody["token"];
                $this->userId = $this->getUserId($cryptedToken);

                $openedDay = $this->userBody["openedDay"];
                if ($this->isDayUnlocked($openedDay)) {
                    $this->addToOpenedDays($openedDay);
                    $this->returnSuccessResponse($this->getGiftData($this->userId, $openedDay));
                } else {
                    $this->returnErrorResponse("Day is locked !");
                }
            } else {
                $this->addLog("notice", "Try to connect with invalid token", array(
                    "cmd"        => $this->cmdName,
                    "givenToken" => $cryptedToken,
                ));

                $this->returnErrorResponse("Invalid token");
            }

            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function isDayUnlocked(int $day)
    {
        if (date("n") !== "12") { // if month is not december
            return FALSE; // no days are unlocked
        }

        return in_array($day, range(1, date("d")));
    }

    protected function getGiftIdByDate(string $userId, int $day)
    {
        $giftIdResponse = $this->adventsKalenderDB->get("SELECT gift_id FROM user_gift_map WHERE user_id = :userId AND day = :day", array(
            "userId" => $userId,
            "day"    => $day,
        ));

        if (isset($giftIdResponse[0]['gift_id'])) {
            return $giftIdResponse[0]['gift_id'];
        }

        return "gift_404";
    }

    protected function getGiftData(string $userId, int $day)
    {
        $giftId = $this->getGiftIdByDate($userId, $day);

        $giftDataResponse = $this->adventsKalenderDB->get("SELECT template, sponsor, text, img FROM gift WHERE id = :id", array(
            "id" => $giftId,
        ));

        $giftData = $giftDataResponse[0];

        $giftSponsorResponse = $this->adventsKalenderDB->get("SELECT name, link, image FROM sponsor WHERE name = :name", array(
            "name" => $giftData["sponsor"],
        ));

        return array(
            "id"           => $giftId,
            "template"     => $giftData["template"],
            "text"         => $giftData["text"],
            "img"          => $this->getAbsolutePathToGiftImages($giftData["img"]),
            "sponsor"      => $this->getSponsorWithAbsolutePathToImage($giftSponsorResponse[0]),
            "wasRecovered" => FALSE, // TODO: Get data from database
        );
    }

    protected function getSponsorWithAbsolutePathToImage(array $sponsor): array
    {
        $sponsor["image"] = $this->apiSettings->getPathToImages() . 'sponsor/' . $sponsor["image"];
        return $sponsor;
    }

    protected function getHashedImgName($imgs)
    {
        $hashedImgsId = array();
        $formatedImgsId = json_decode($imgs);

        foreach ($formatedImgsId as $imgId) {
            if (Helper::isSet($imgId)) {
                $hashedImgsId[$imgId]["name"] = $this->getImgHashedName($imgId);
            }
        }

        return json_encode($hashedImgsId);
    }

    protected function getAbsolutePathToGiftImages(string $imgs): string
    {
        $pathToImgs = $this->apiSettings->getPathToImages();
        $parsedImgs = json_decode($imgs);

        foreach ($parsedImgs as &$img) {
            $img = $pathToImgs . 'gift/' . $img;
        }

        return json_encode($parsedImgs);
    }

    protected function addToOpenedDays(int $day)
    {
        $openedDaysResponse = $this->adventsKalenderDB->get("SELECT opened_days FROM user WHERE id = :id", array(
            "id" => $this->userId,
        ));

        $openedDays = $openedDaysResponse[0]["opened_days"];

        if ($openedDays === "") {
            $openedDays = [];
        } else {
            $openedDays = explode("-", $openedDays);
        }

        if (!in_array(strval($day), $openedDays)) {
            array_push($openedDays, strval($day));

            $this->adventsKalenderDB->query(
                "UPDATE user SET opened_days = :openedDays WHERE id = :userId",
                array(
                    "userId"     => $this->userId,
                    "openedDays" => join("-", $openedDays),
                )
            );
        }
    }
}

$getDayGiftDataClass = new GetDayGiftData();
$getDayGiftDataClass->request();
