<?php

require_once "../../request.php";

class CreateUserAwaitingConfirmation extends Request implements IRequest
{
	protected $cmdName = "CreateUserAwaitingConfirmation";

	public function request()
	{

		$this->getRequestData();

		try {
			$this->adventsKalenderDB->connect();

			$isMailPresentInUser_to_check = $this->adventsKalenderDB->isPresent("SELECT * FROM user_to_check WHERE email= :email", array(
				"email" => $this->userBody["email"],
			));

			$isMailPresentInUser = $this->adventsKalenderDB->isPresent("SELECT * FROM user WHERE email= :email", array(
				"email" => $this->userBody["email"],
			));

			$isMailPresentInBeneficiaries = $this->adventsKalenderDB->isPresent("SELECT * FROM beneficiaries WHERE email= :email", array(
				"email" => $this->userBody["email"],
			));

			if (!$isMailPresentInBeneficiaries) {
				$this->returnSuccessResponse(
					array(
						"hasUserBeenCreated" => FALSE,
						"error"          => "notBeneficiary",
					)
				);
			} elseif ($isMailPresentInUser_to_check) {
				$this->returnSuccessResponse(
					array(
						"hasUserBeenCreated" => FALSE,
						"error"              => "accountInCreation",
					)
				);
			} elseif ($isMailPresentInUser) {
				$this->returnSuccessResponse(
					array(
						"hasUserBeenCreated" => FALSE,
						"error"          => "accountExist",
					)
				);
			} else {
				$userAwaitingConfirmationId = Helper::getId();

				$emailConstructor = array(
					"email"			  =>  array($this->userBody["email"]),
					"subject"		  => "Account creation : ISFATES Adventskalender",
					"bodyConstructor" => array(
						"pathToTemplate" => "./validEmailTemplate.html",
						"replacer"		 => array(
							"__USER_FIRST_NAME__"  => $this->userBody["first_name"],
							"__PATH_TO_API__"      => $this->apiSettings->getPathToApi(),
							"__USER_TO_VALID_ID__" => $userAwaitingConfirmationId,
						),
					)
				);

				$this->emailSender->sendEmail($emailConstructor);

				if ($this->emailSender->hasEmailBeenSend) {

					$this->insertUserToCheck(
						$userAwaitingConfirmationId,
						$this->userBody["email"],
						$this->userBody["name"],
						$this->userBody["first_name"],
						$this->userBody["gender"],
						$this->userBody["password"],
						$this->userBody["language"]
					);

					$this->returnSuccessResponse(
						array(
							"hasUserBeenCreated" => TRUE,
						)
					);
				} else {
					$this->returnErrorResponse("Impossible to send Email !");
				}
			}

			$this->adventsKalenderDB->disconnect();
		} catch (Exception $e) {
			$this->addLog("debug", "Critical error", array(
				"cmd"   => $this->cmdName,
				"error" => $e->getMessage(),
			));

			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function insertUserToCheck(string $userId, string $email, string $name, string $first_name, string $gender, string $pwd, string $lang)
	{
		$this->adventsKalenderDB->query(
			"INSERT INTO user_to_check(id, email, name, first_name, gender, pwd, lang) VALUES(:id, :email, :name, :first_name, :gender, :pwd, :lang)",
			array(
				"id"         => $userId,
				"email"      => $email,
				"name"       => $name,
				"first_name" => $first_name,
				"gender"     => $gender,
				"pwd"        => $this->myCrypto->hashPassword($pwd),
				"lang"       => $lang,
			)
		);
	}
}

$createUserAwaitingConfirmation = new CreateUserAwaitingConfirmation();
$createUserAwaitingConfirmation->request();
