<?php

require_once "../../request.php";

class GetUserData extends Request implements IRequest
{
    protected $cmdName = "GetUserData";
    protected $userIdToCreate;

    public function request()
    {

        try {
            $this->getRequestData();

            $this->adventsKalenderDB->connect();

            if ($this->isTokenValid($this->userBody["token"])) {
                $cryptedToken = $this->userBody["token"];
                $token = $this->getDecryptedToken($cryptedToken);

                $getUserIdResponse = $this->adventsKalenderDB->get("SELECT user_id FROM token WHERE id= :id", array(
                    "id" => $token,
                ));

                $userId = $getUserIdResponse[0]["user_id"];

                $userData = $this->adventsKalenderDB->get("SELECT pseudo, email, name, first_name, days_pattern, opened_days, lang, avatar FROM user WHERE id= :id", array(
                    "id" => $userId,
                ));

                $this->logGetUserData($userId);

                $this->returnSuccessResponse(
                    array(
                        "pseudo"       => $userData[0]["pseudo"],
                        "email"        => $userData[0]["email"],
                        "name"         => $userData[0]["name"],
                        "first_name"   => $userData[0]["first_name"],
                        "daysPattern"  => array_map('intval', explode("-", $userData[0]["days_pattern"])),
                        "openedDays"   => $this->getOpenedDaysFromString($userData[0]["opened_days"]),
                        "lang"         => $userData[0]["lang"],
                        "unlockedDays" => $this->getUnlockedDays(),
                        "avatarLink"   => "https://johannchopin.fr/img/adventskalender/avatar/avatar1.svg",
                    )
                );
            } else {
                $this->addLog("notice", "Try to connect with invalid token", array(
                    "cmd"        => $this->cmdName,
                    "givenToken" => $this->userBody["token"],
                ));

                $this->returnErrorResponse("Invalid token");
            }


            $this->adventsKalenderDB->disconnect();
        } catch (Exception $e) {
            $this->addLog("critical", "Critical error", array(
                "cmd"   => $this->cmdName,
                "error" => $e->getMessage(),
            ));

            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function getOpenedDaysFromString($openedDays)
    {
        // TODO: Refactoring this sh*t !!!
        $result_array = array();
        $strings_array = explode("-", $openedDays);

        if ($strings_array === [""]) {
            return [];
        }

        foreach ($strings_array as $each_number) {
            $result_array[] = (int) $each_number;
        }

        return $result_array;
    }

    protected function logGetUserData(string $userId)
    {
        $this->addLog("info", "User get data", array(
            "cmd"    => $this->cmdName,
            "userId" => $userId,
        ));
    }

    protected function getUnlockedDays()
    {
        if (date("n") !== "12") { // if month is not december
            return array(); // no days are unlocked
        }

        return range(1, date("d"));
    }

    protected function getAvatarLink(string $avatarPath)
    {
        // TODO: Add path to images to api-settings
        return "https://johannchopin.fr/img/adventskalender/avatar/" . $avatarPath;
    }
}

$getUserDataClass = new GetUserData();
$getUserDataClass->request();
