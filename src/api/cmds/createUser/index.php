<?php

require_once "../../request.php";

class CreateUser extends Request implements IRequest
{
	protected $cmdName = "CreateUser";
	protected $userIdToCreate;

	protected function getRandomDayPattern()
	{
		$daysInArray = range(1, 24);
		shuffle($daysInArray);

		return join("-", $daysInArray);
	}

	protected function getPseudoFromEmail(string $email)
	{
		$explodedEmail = explode("@", $email);

		return $explodedEmail[0];
	}

	public function request()
	{

		try {
			$this->userIdToCreate = $_GET["id"];

			$this->adventsKalenderDB->connect();

			if ($this->adventsKalenderDB->isPresent("SELECT * FROM user_to_check WHERE id= :userIdToCreate", array(
				"userIdToCreate" => $this->userIdToCreate,
			))) {

				$userData = $this->adventsKalenderDB->get("SELECT * FROM user_to_check WHERE id= :userIdToCreate", array(
					"userIdToCreate" => $this->userIdToCreate,
				));

				$userData = $userData[0];

				$userId = $this->generateUserId();

				$this->adventsKalenderDB->query(
					"INSERT INTO user(id, pseudo, email, name, first_name, gender, pwd, days_pattern, lang, avatar) VALUES(:id, :pseudo, :email, :name, :first_name, :gender, :pwd, :days_pattern, :lang, :avatar)",
					array(
						"id"    	   => $userId,
						"pseudo" 	   => $this->getPseudoFromEmail($userData["email"]),
						"email" 	   => $userData["email"],
						"name"         => $userData["name"],
						"first_name"   => $userData["first_name"],
						"gender"       => $userData["gender"],
						"pwd"   	   => $userData["pwd"],
						"days_pattern" => $this->getRandomDayPattern(),
						"avatar" 	   => "avatar1.svg",
						//"avatar" 	   => $this->getUserAvatarLink(),
						"lang" 		   => $userData["lang"],
					)
				);

				$this->adventsKalenderDB->query(
					"DELETE FROM user_to_check WHERE id=:userIdToCreate",
					array(
						"userIdToCreate" => $this->userIdToCreate,
					)
				);

				$this->logUserCreation($userId);

				Helper::redirectToLink("success.html");
			} else {
				Helper::redirectToLink("error.html");
			}

			$this->adventsKalenderDB->disconnect();
		} catch (Exception $e) {
			$this->addLog("critical", "Critical error", array(
				"cmd"   => $this->cmdName,
				"error" => $e->getMessage(),
			));

			Helper::redirectToLink("error.html");
		}
	}

	protected function getUserAvatarLink(): string
	{
		$avatarNumber = rand(1, 4);

		return "avatar{$avatarNumber}.svg";
	}

	protected function logUserCreation(string $userId)
	{
		$this->addLog("info", "User creation", array(
			"cmd"    => $this->cmdName,
			"userId" => $userId,
		));
	}

	protected function generateUserId()
	{
		$generatedId = Helper::getId();

		if ($this->doesUserIdAlreadyExist($generatedId)) {
			return $this->generateUserId();
		}

		return $generatedId;
	}

	protected function doesUserIdAlreadyExist(string $userId)
	{
		return $this->adventsKalenderDB->isPresent("SELECT * FROM user WHERE id= :idToCheck", array(
			"idToCheck" => $userId,
		));
	}
}

$createUser = new CreateUser();
$createUser->request();
