import time
from driveApiConnexion import DriveApi
from cliHandler import CliHandler


class GetGivenGiftsData(DriveApi, CliHandler):

    def __init__(self):
        DriveApi.__init__(self)
        CliHandler.__init__(self)

    def getDataFromCol(self, sheet, col, rowToStart=0):
        fullCol = sheet.col_values(col)

        return fullCol[rowToStart:]

    def removeEmptyElmtInArray(self, arr):
        return ' '.join(arr).split()

    def getRightElmtOfLineTitle(self, sheet, lineTitle):
        matchingCell = sheet.find(lineTitle)
        return sheet.cell(matchingCell.row, matchingCell.col + 1).value

    def getGiftTextData(self, sheet):
        textData = {
            "en": {},
            "fr": {},
            "de": {},
        }
        textDataStartCell = sheet.find("TEXT_DATA")
        textDataStartCellCol = textDataStartCell.col
        textDataEndCell = sheet.find("END_TEXT_DATA")

        EN_LANG_COL = textDataStartCellCol + 2
        FR_LANG_COL = textDataStartCellCol + 3
        DE_LANG_COL = textDataStartCellCol + 4

        for row in range(textDataStartCell.row + 1, textDataEndCell.row):
            lineTitle = sheet.cell(row, textDataStartCellCol + 1).value
            textData["en"][lineTitle] = sheet.cell(row, EN_LANG_COL).value
            textData["fr"][lineTitle] = sheet.cell(row, FR_LANG_COL).value
            textData["de"][lineTitle] = sheet.cell(row, DE_LANG_COL).value

        return textData

    def getGiftImgData(self, sheet):
        imgData = {}
        imgDataStartCell = sheet.find("IMG_DATA")
        imgDataStartCellCol = imgDataStartCell.col
        imgDataEndCell = sheet.find("END_IMG_DATA")

        KEY_COL = imgDataStartCellCol + 1
        NAME_COL = imgDataStartCellCol + 2
        # DESCRIPTION_COL = imgDataStartCellCol + 3

        # imgDataStartCell.row + 2 -> because we skip columns title
        for row in range(imgDataStartCell.row + 2, imgDataEndCell.row):
            imgKey = sheet.cell(row, KEY_COL).value
            imgName = sheet.cell(row, NAME_COL).value
            # Don't need to save description
            # imgDescription = sheet.cell(row, DESCRIPTION_COL).value

            imgData[imgKey] = imgName

        return imgData

    def getData(self):
        giftsName = self.scriptParams["giftsName"]
        giftsData = {}

        for giftName in giftsName:
            sheet = self.client.open(giftName).sheet1
            giftsData[giftName] = {}
            giftsData[giftName]["name"] = self.getRightElmtOfLineTitle(
                sheet, "NAME")
            giftsData[giftName]["id"] = self.getRightElmtOfLineTitle(
                sheet, "ID")
            giftsData[giftName]["value"] = self.getRightElmtOfLineTitle(
                sheet, "VALUE")
            giftsData[giftName]["sexe"] = self.getRightElmtOfLineTitle(
                sheet, "GENDER")
            giftsData[giftName]["amount"] = self.getRightElmtOfLineTitle(
                sheet, "AMOUNT")
            giftsData[giftName]["template"] = self.getRightElmtOfLineTitle(
                sheet, "TEMPLATE")
            giftsData[giftName]["sponsor"] = self.getRightElmtOfLineTitle(
                sheet, "SPONSOR")
            giftsData[giftName]["text"] = self.getGiftTextData(sheet)
            giftsData[giftName]["img"] = self.getGiftImgData(sheet)

            # Wait for drive API
            # TODO: Wait more for Drive API
            time.sleep(1)

        self.returnJson(giftsData)


GetGivenGiftsData().getData()
