from driveApiConnexion import DriveApi
from cliHandler import CliHandler


class GetSponsorsData(DriveApi, CliHandler):

    def __init__(self):
        DriveApi.__init__(self)
        CliHandler.__init__(self)

    def getDataFromCol(self, sheet, col, rowToStart=0):
        fullCol = sheet.col_values(col)

        return fullCol[rowToStart:]

    def getData(self):
        sheet = self.client.open("sponsors_list").sheet1

        nameCell = sheet.find("name")
        linkCellCol = sheet.find("link").col
        imageCellCol = sheet.find("image").col

        sponsors = {}

        currentSponsorRow = nameCell.row + 1

        while sheet.cell(currentSponsorRow, nameCell.col).value != "":
            sponsors[currentSponsorRow] = {
                "name": sheet.cell(currentSponsorRow, nameCell.col).value,
                "link": sheet.cell(currentSponsorRow, linkCellCol).value,
                "image": sheet.cell(currentSponsorRow, imageCellCol).value
            }

            currentSponsorRow += 1

        self.returnJson(sponsors)


GetSponsorsData().getData()
