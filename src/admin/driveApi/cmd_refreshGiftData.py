from driveApiConnexion import DriveApi
from cliHandler import CliHandler


class RefreshGiftData(DriveApi, CliHandler):

    def __init__(self):
        DriveApi.__init__(self)
        CliHandler.__init__(self)

    def getData(self):
        sheet = self.client.open("gift_test").sheet1
        self.returnJson(sheet.row_values(1))


RefreshGiftData().getData()
