import os
import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials


class DriveApi:
    'Basis class to connect to Drive Api'

    pathToApiSettings = '../api-settings.json'

    def getApiSettingsAbsolutePath(self):
        return os.path.join(os.path.dirname(__file__), self.pathToApiSettings)

    def getApiSettings(self):
        with open(self.getApiSettingsAbsolutePath()) as apiSettings:
            return json.load(apiSettings)

    def getCredentialsAbsolutPath(self):
        apiSettings = self.getApiSettings()
        return apiSettings["drive_api"]["credentials_path"]

    def __init__(self):
        self.scope = ['https://www.googleapis.com/auth/spreadsheets',
                      "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]
        self.creds = ServiceAccountCredentials.from_json_keyfile_name(
            self.getCredentialsAbsolutPath(), self.scope)
        self.client = gspread.authorize(self.creds)

    def returnJson(self, array):
        print(json.dumps(array))
