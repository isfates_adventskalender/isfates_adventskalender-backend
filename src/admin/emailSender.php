<?php

require_once dirname(__FILE__) . "/../vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;


class EmailSender
{
    protected $mailHandler = "adventskalender.isfates@gmail.com";
    protected $gmailCredentials;
    protected $fctToExecuteOnSendEmailError;

    public $hasEmailBeenSend = FALSE;

    public function __construct(array $settings, closure $fctToExecuteOnSendEmailError)
    {
        $this->gmailCredentials = Helper::getJsonFromFile($settings["gmail_api_credentials"]);
        $this->fctToExecuteOnSendEmailError = $fctToExecuteOnSendEmailError;
    }

    public function sendEmail($emailConstructor)
    {
        //Create a new PHPMailer instance
        $mail = new PHPMailer(TRUE);

        $mail->isSMTP();



        // TODO: !!!! IMPORTANT REMOVE THIS CODE ONLINE PRODUCTION TO ... !!!!!
        // It's here to send mail on localhost to avoid some Troubleshooting
        // For example -> avast doesn't allow to send mail 
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            )
        );
        // TODO: !!!! ... HERE !!!!!



        //Set the hostname of the mail server
        $mail->Host = "smtp.gmail.com";
        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;
        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = "tls";
        //Whether to use SMTP authentication
        $mail->SMTPAuth = TRUE;
        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = $this->gmailCredentials["mail"];
        //Password to use for SMTP authentication
        $mail->Password = $this->gmailCredentials["password"];

        //Set who the message is to be sent from
        if (isset($emailConstructor["senderEmail"])) {
            $mail->setFrom($emailConstructor["senderEmail"]);
        } else {
            $mail->setFrom($this->mailHandler, "ISFATES-Adventskalender");
        }

        //Set who the message is to be sent to
        foreach ($emailConstructor["email"] as $email) {
            $mail->addAddress($email);
        }

        //Set the subject line
        $mail->isHTML(TRUE); // Set email format to HTML
        $mail->Subject = $emailConstructor["subject"];
        $mail->Body    = $this->getHTMLEmailWithParams($emailConstructor["bodyConstructor"]["pathToTemplate"], $emailConstructor["bodyConstructor"]["replacer"]);
        $mail->AltBody = "Html does\"nt work"; //TODO: Refactoring

        //send the message, check for errors
        if ($mail->send()) {
            $this->hasEmailBeenSend = TRUE;
        } else {
            ($this->fctToExecuteOnSendEmailError)($mail->ErrorInfo);
        }
    }

    protected function getHTMLEmailWithParams(string $pathToTemplate, array $params)
    {
        $html = file_get_contents($pathToTemplate);

        foreach ($params as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

        return $html;
    }
}
