<?php

require_once __DIR__ . '/../../request.php';

class AssignGiftToUser extends Request implements IRequest
{

    public function request()
    {
        $this->getRequestData();

        try {
            $this->assignGiftToUserRequest();
        } catch (Exception $e) {
            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function assignGiftToUserRequest()
    {
        $day = $this->userBody["day"];
        $userId = $this->userBody["user_id"];
        $giftId = $this->userBody["gift_id"];

        $this->adventsKalenderDB->connect();

        if ($userId === "--ALL--") {
            $this->attributeGiftToAllUsers($giftId, $day);
        } elseif ($userId === "--RANDOM--") {
            $this->attributeGiftToRandomUser($giftId, $day);
        } else {
            $this->attributeGiftToUser($userId, $giftId, $day);
        }

        $this->adventsKalenderDB->disconnect();

        $this->returnSuccessResponse(
            array(
                "hasGiftBeenAssigned" => TRUE,
            )
        );
    }

    protected function attributeGiftToRandomUser(string $giftId, int $day)
    {
        $winnerId = $this->getRandomUserIdThatHaveNotWin();

        $this->attributeGiftToUser($winnerId, $giftId, $day);
    }

    protected function getRandomUserIdThatHaveNotWin(): string
    {
        $potentialWinnerIds = $this->getPotentialsWinnerIds();
        $potentialWinnerAmount = sizeof($potentialWinnerIds);

        return $potentialWinnerIds[rand(0, $potentialWinnerAmount - 1)];
    }

    protected function getPotentialsWinnerIds(): array
    {
        // TODO: Refactoring...
        $potentialWinnerIds = $this->adventsKalenderDB->get("SELECT id FROM user");

        $userIdsThatHaveWonBigGift = $this->adventsKalenderDB->get("SELECT DISTINCT user_id FROM user_gift_map WHERE gift_id IN (SELECT id FROM gift WHERE value < 5)");

        foreach ($potentialWinnerIds as $potentialWinnerIdKey => $potentialWinnerId) {
            foreach ($userIdsThatHaveWonBigGift as $userIdThatHaveWonBigGift) {
                if ($potentialWinnerId["id"] === $userIdThatHaveWonBigGift["user_id"]) {
                    // Remove user id from potential winner if he has allready won
                    unset($potentialWinnerIds[$potentialWinnerIdKey]);
                }
            }
        }

        if ($potentialWinnerIds === array()) {
            $sanitedPotentialWinnerIds = array();

            foreach ($this->adventsKalenderDB->get("SELECT id FROM user") as $userId) {
                array_push($sanitedPotentialWinnerIds, $userId["id"]);
            }

            return $sanitedPotentialWinnerIds;
        }


        // Get only ids in array()
        $sanitedPotentialWinnerIds = array();

        foreach ($potentialWinnerIds as $potentialWinnerId) {
            array_push($sanitedPotentialWinnerIds, $potentialWinnerId["id"]);
        }


        return $sanitedPotentialWinnerIds;
    }

    protected function attributeGiftToAllUsers(string $giftId, int $day)
    {
        $allUsersIdResponse = $this->adventsKalenderDB->get("SELECT id FROM user");

        foreach ($allUsersIdResponse as $userIdResponse) {
            $this->attributeGiftToUser($userIdResponse['id'], $giftId, $day);
        }
    }

    protected function attributeGiftToUser(string $userId, string $giftId, int $day)
    {
        $hasUserAllreadyGift = $this->adventsKalenderDB->isPresent("SELECT user_id FROM user_gift_map WHERE user_id = :userId AND day = :day", array(
            "userId" => $userId,
            "day"    => $day,
        ));

        if (!$hasUserAllreadyGift) {
            $this->adventsKalenderDB->query("INSERT user_gift_map(user_id, gift_id, day, was_recovered) VALUES(:userId, :giftId, :day, :wasRecovered)", array(
                "userId"       => $userId,
                "giftId"       => $giftId,
                "day"          => $day,
                "wasRecovered" => FALSE,
            ));
        } else {
            $this->adventsKalenderDB->query("UPDATE user_gift_map SET gift_id = :giftId WHERE user_id = :userId AND day = :day", array(
                "userId"       => $userId,
                "giftId"       => $giftId,
                "day"          => $day,
            ));
        }
    }
}


$assignGiftToUserClass = new AssignGiftToUser();
$assignGiftToUserClass->request();
