<?php

require_once '../../request.php';

class SynchronizeSponsors extends Request implements IRequest
{

	public function request()
	{
		$this->getRequestData();

		try {
			$this->synchronizeSponsorsRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function synchronizeSponsorsRequest()
	{
		$sponsors = $this->getPythonAPICmdResponse("getSponsorsData");

		$this->adventsKalenderDB->connect();

		$this->insertSponsorsInDb($sponsors);

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"hasSponsorsBeenSynchronized" => TRUE,
			)
		);
	}

	protected function insertSponsorsInDb(array $sponsors)
	{
		foreach ($sponsors as $sponsor) {
			$isSponsorPresent = $this->adventsKalenderDB->isPresent("SELECT name FROM sponsor WHERE name = :name", array(
				"name" => $sponsor["name"]
			));

			if ($isSponsorPresent) {
				$this->adventsKalenderDB->query("UPDATE sponsor SET link = :link, image = :image WHERE name = :name", array(
					"name"  => $sponsor["name"],
					"link"  => $sponsor["link"],
					"image" => $sponsor["image"]
				));
			} else {
				$this->adventsKalenderDB->query("INSERT INTO sponsor(name, link, image) VALUES (:name, :link, :image)", array(
					"name"  => $sponsor["name"],
					"link"  => $sponsor["link"],
					"image" => $sponsor["image"]
				));
			}
		}
	}
}


$synchronizeSponsorsClass = new SynchronizeSponsors();
$synchronizeSponsorsClass->request();
