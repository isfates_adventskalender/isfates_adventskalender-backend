<?php

require_once __DIR__ . '/../../request.php';

class GetUsersId extends Request implements IRequest
{

	public function request()
	{
		try {
			$this->getUsersIdRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function getUsersIdRequest()
	{
		$ids = array();

		$this->adventsKalenderDB->connect();

		$getIdsResponse = $this->adventsKalenderDB->get("SELECT id FROM user");

		foreach ($getIdsResponse as $userId) {
			array_push($ids, $userId["id"]);
		}

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"ids" => $ids,
			)
		);
	}
}


$getUsersIdClass = new GetUsersId();
$getUsersIdClass->request();
