<?php

require_once '../../request.php';

class GetTestAccounts extends Request implements IRequest
{

	public function request()
	{

		try {
			$logFilesName = [];

			// TODO: Finish GetTestAccounts cmd
			$this->returnSuccessResponse(
				array(
					"testAccounts" => '$this->getTestAccountsFromDb()',
				)
			);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function getTestAccountsFromDb()
	{
		$testAccounts = $this->adventsKalenderDB->get("SELECT id, pseudo, email FROM user WHERE id REGEXP '^testAccount-'");

		return $testAccounts;
	}
}


$getTestAccountsClass = new GetTestAccounts();
$getTestAccountsClass->request();
