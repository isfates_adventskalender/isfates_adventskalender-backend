<?php

require '../../request.php';

class AddTestAccount extends Request implements IRequest
{
	protected $generatedUserId;
	protected $userEmail;

	public function __construct()
	{
		parent::__construct();
	}

	public function request()
	{
		$this->getRequestData();

		try {
			$this->addTestAccountRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function addTestAccountRequest()
	{
		$this->adventsKalenderDB->connect();

		$this->userEmail = $this->userBody["username"];
		$this->generatedUserId = "testAccount-" . $this->userBody["username"];

		$this->addTestAccountInDb($this->userBody["password"]);
		$this->updateTestAccountId();

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"hasTestAccountBeAdd" => TRUE,
			)
		);
	}

	protected function addTestAccountInDb(string $pwd)
	{
		$this->adventsKalenderDB->query(
			"INSERT INTO user_to_check(id, email, pwd) VALUES(:id, :email, :pwd)",
			array(
				"id"    => $this->generatedUserId,
				"email" => $this->userEmail,
				"pwd"   => $this->myCrypto->hashPassword($pwd),
			)
		);

		$linkToValidateAccount = "https://api.johannchopin.fr/advents_kalender/api/cmds/createUser/index.php?id=" . urlencode($this->generatedUserId);

		$this->openLinkInBackground($linkToValidateAccount);
	}

	protected function updateTestAccountId()
	{
		$this->adventsKalenderDB->query(
			"UPDATE user SET id = :id WHERE email = :email",
			array(
				"id"      => $this->generatedUserId,
				"email" => $this->generatedUserId,
			)
		);
	}

	protected function openLinkInBackground(string $link)
	{
		// create a new cURL resource
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		// grab URL and pass it to the browser
		curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);
	}
}


$addTestAccountClass = new AddTestAccount();
$addTestAccountClass->request();
