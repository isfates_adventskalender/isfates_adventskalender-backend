<?php

require '../../request.php';

class SynchronizeGift extends Request implements IRequest
{
	public function request()
	{
		$this->getRequestData();

		$giftsName = $this->userBody["giftsName"];

		try {
			$this->synchronizeGiftRequest($giftsName);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function synchronizeGiftRequest(array $giftsName)
	{
		$giftsData = $this->getPythonAPICmdResponse("getGivenGiftsData", array(
			"giftsName" => $giftsName,
		));

		$this->adventsKalenderDB->connect();

		foreach ($giftsData as $giftData) {
			$this->sendGiftDataToDb($giftData['id'], $giftData);
		}

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"haveGiftsBeenSynchronized" => TRUE,
			)
		);
	}

	protected function sendGiftDataToDb(string $giftId, array $giftData)
	{
		if ($this->adventsKalenderDB->isPresent("SELECT id FROM gift WHERE id= :id", array(
			"id" => $giftId,
		))) {
			$this->adventsKalenderDB->query(
				"UPDATE gift SET name = :name, value = :value, gender = :gender, amount = :amount, template = :template, sponsor = :sponsor, text = :text, img = :img WHERE id = :id",
				array(
					"id"       => $giftId,
					"name"     => $giftData["name"],
					"value"    => $giftData["value"],
					"gender"   => $this->getReducedSexeValue($giftData["sexe"]),
					"amount"   => $giftData["amount"],
					"template" => $giftData["template"],
					"sponsor"  => $giftData["sponsor"],
					"text"     => json_encode($giftData["text"]),
					"img"      => json_encode($giftData["img"]),
				)
			);
		} else {
			$this->adventsKalenderDB->query(
				"INSERT INTO gift(id, name, value, gender, amount, template, sponsor, text, img) VALUES(:id, :name, :value, :gender, :amount, :template, :sponsor, :text, :img)",
				array(
					"id"       => $giftId,
					"name"     => $giftData["name"],
					"value"    => $giftData["value"],
					"gender"   => $this->getReducedSexeValue($giftData["sexe"]),
					"amount"   => $giftData["amount"],
					"template" => $giftData["template"],
					"sponsor"  => $giftData["sponsor"],
					"text"     => json_encode($giftData["text"]),
					"img"      => json_encode($giftData["img"]),
				)
			);
		}
	}

	protected function getReducedSexeValue(string $sexe)
	{
		switch ($sexe) {
			case "MIXTE":
				return "m";
			case "FEMME":
				return "f";
			case "HOMME":
				return "h";
		}
	}
}


$synchronizeGiftClass = new SynchronizeGift();
$synchronizeGiftClass->request();
