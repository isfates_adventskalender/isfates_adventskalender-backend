<?php

require_once __DIR__ . '/../../request.php';

class GetGiftsId extends Request implements IRequest
{

	public function request()
	{
		try {
			$this->getUsersIdRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function getUsersIdRequest()
	{
		$ids = array();

		$this->adventsKalenderDB->connect();

		$getIdsResponse = $this->adventsKalenderDB->get("SELECT id FROM gift WHERE 
		amount > (SELECT COUNT(gift_id) FROM user_gift_map WHERE gift_id = gift.id) 
		OR amount = '-'");

		foreach ($getIdsResponse as $userId) {
			array_push($ids, $userId["id"]);
		}

		$this->adventsKalenderDB->disconnect();

		$this->returnSuccessResponse(
			array(
				"ids" => $ids,
			)
		);
	}
}


$getGiftsIdClass = new GetGiftsId();
$getGiftsIdClass->request();
