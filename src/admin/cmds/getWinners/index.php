<?php

require_once __DIR__ . '/../../request.php';

class GetWinners extends Request implements IRequest
{

	public function request()
	{
		$this->getRequestData();

		try {
			$this->adventsKalenderDB->connect();

			$winners = array();

			$winnersResponse = $this->adventsKalenderDB->get("SELECT user_id, gift_id, day FROM user_gift_map WHERE gift_id IN 
			(SELECT id FROM gift WHERE value < 5)");

			foreach ($winnersResponse as $winnerResponse) {
				$userDataResponse =  $this->adventsKalenderDB->get("SELECT email, name, first_name FROM user WHERE id = :userId", array(
					"userId" => $winnerResponse["user_id"]
				));

				$userData = $userDataResponse[0];

				$giftDataResponse =  $this->adventsKalenderDB->get("SELECT name FROM gift WHERE id = :giftId", array(
					"giftId" => $winnerResponse["gift_id"]
				));

				$giftData = $giftDataResponse[0];

				$winner = array(
					"name"       => $userData["name"],
					"first_name" => $userData["first_name"],
					"email"      => $userData["email"],
					"day"        => $winnerResponse["day"],
					"gift_id"    => $winnerResponse["gift_id"],
					"gift_name"  => $giftData["name"],
					"is_open"    => $this->isGiftOpen($winnerResponse["user_id"], $winnerResponse["day"]),
				);

				array_push($winners, $winner);
			}

			$this->adventsKalenderDB->disconnect();

			$this->returnSuccessResponse(
				array(
					'winners' => $winners,
				)
			);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function isGiftOpen(string $userId, string $day): bool
	{
		$userOpenedDaysResponse = $this->adventsKalenderDB->get("SELECT opened_days FROM user WHERE id = :userId", array(
			"userId" => $userId,
		));

		$userOpenedDays = explode("-", $userOpenedDaysResponse[0]["opened_days"]);

		return in_array($day, $userOpenedDays);
	}
}


$getWinnersClass = new GetWinners();
$getWinnersClass->request();
