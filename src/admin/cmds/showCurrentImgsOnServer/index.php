<?php

require '../../request.php';

class ShowCurrentImgsOnServer extends Request implements IRequest
{

	public function request()
	{

		try {
			$imgsUrl = [];
			foreach (glob("../../../../../johannchopin.fr/img/adventskalender/*") as $file) {
				array_push($imgsUrl, str_replace("../../../../../", "https://", $file));
			}

			$this->returnSuccessResponse(
				array(
					"imgs" => $imgsUrl,
				)
			);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}
}


$showCurrentImgsOnServerClass = new ShowCurrentImgsOnServer();
$showCurrentImgsOnServerClass->request();
