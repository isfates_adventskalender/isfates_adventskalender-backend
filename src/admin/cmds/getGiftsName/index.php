<?php

require '../../request.php';

class GetGiftsName extends Request implements IRequest
{

	public function request()
	{
		$this->getRequestData();

		try {
			$this->getGiftsNameRequest();
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}

	protected function getGiftsNameRequest()
	{
		$giftsName = $this->getPythonAPICmdResponse("getGiftsName");

		$this->returnSuccessResponse(
			array(
				"giftsName" => $giftsName,
			)
		);
	}
}


$synchronizeBeneficiariesClass = new GetGiftsName();
$synchronizeBeneficiariesClass->request();
