<?php

require '../../request.php';

class GetLogs extends Request implements IRequest
{

	public function request()
	{
		$this->getRequestData();

		$logsFileName = $this->userBody['logsFileName'];

		try {

			$logs = file_get_contents($this->API_SETTINGS["logs_handler"]["logs_folder_path"] . $logsFileName);

			$this->returnSuccessResponse(
				array(
					'logs' => $logs,
				)
			);
		} catch (Exception $e) {
			$this->returnErrorResponse($e->getMessage());
		}
	}
}


$getLogsClass = new GetLogs();
$getLogsClass->request();
