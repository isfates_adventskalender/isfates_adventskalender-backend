<?php

require_once '../../request.php';

class GetImgsId extends Request implements IRequest
{

    public function request()
    {
        $this->getRequestData();

        try {
            $this->getImgsIdRequest();
        } catch (Exception $e) {
            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function getImgsIdRequest()
    {

        $this->adventsKalenderDB->connect();

        $imgsName = $this->getImgsIdFromDb();

        $this->adventsKalenderDB->disconnect();

        $this->returnSuccessResponse(
            array(
                "imgs" => $imgsName,
            )
        );
    }

    protected function getImgsIdFromDb()
    {
        $imgsName = array();
        $getImgsIdResponse = $this->adventsKalenderDB->get("SELECT id FROM image");

        foreach ($getImgsIdResponse as $imgsNameResponse) {
            array_push($imgsName, $imgsNameResponse["id"]);
        }

        return $imgsName;
    }
}


$getImgsIdClass = new GetImgsId();
$getImgsIdClass->request();
