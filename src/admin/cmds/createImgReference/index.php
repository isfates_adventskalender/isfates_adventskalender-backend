<?php

require_once '../../request.php';

class CreateImgReference extends Request implements IRequest
{

    protected $imgId;
    protected $imgType;

    public function request()
    {
        $this->getRequestData();

        $this->imgId = $this->userBody['id'];
        $this->imgType = $this->userBody['type'];

        try {
            $this->adventsKalenderDB->connect();

            if (!($this->doesImgIdAlreadyExist())) {
                $this->createImgReferenceRequest();
            } else {
                $this->returnErrorResponse("Image Id is a duplicate !");
            }
        } catch (Exception $e) {
            $this->returnErrorResponse($e->getMessage());
        }
    }

    protected function createImgReferenceRequest()
    {
        $imgHash = $this->getGeneratedImgHash();

        $this->insertImgReferenceInDb($imgHash);

        $this->adventsKalenderDB->disconnect();

        $this->returnSuccessResponse(
            array(
                "imgHashedName" => $imgHash . $this->imgType,
            )
        );
    }

    protected function getGeneratedImgHash()
    {
        $generatedId = Helper::getId();

        if ($this->doesImgHashAlreadyExist($generatedId)) {
            return $this->generateImgHash();
        }

        return $generatedId;
    }

    protected function doesImgHashAlreadyExist(string $imgHash)
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM image WHERE hashed_name = :imgHash", array(
            "imgHash" => $imgHash,
        ));
    }

    protected function doesImgIdAlreadyExist()
    {
        return $this->adventsKalenderDB->isPresent("SELECT * FROM image WHERE id = :imgId", array(
            "imgId" => $this->imgId,
        ));
    }

    protected function insertImgReferenceInDb(string $imgHash)
    {
        $this->adventsKalenderDB->query(
            "INSERT INTO image(id, hashed_name) VALUES(:id, :hashedName)",
            array(
                "id"         => $this->imgId,
                "hashedName" => $imgHash . $this->imgType,
            )
        );
    }
}


$createImgReferenceClass = new CreateImgReference();
$createImgReferenceClass->request();
