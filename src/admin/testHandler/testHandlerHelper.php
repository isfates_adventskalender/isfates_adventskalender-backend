<?php

require_once dirname(__FILE__) . "/../helper.php";

class TestHandlerHelper extends Helper
{
    public static function doPostRequest(string $url, array $data)
    {
        // TODO: Debug curl error on server api call
        //url-ify the data for the POST
        $fields_string = http_build_query($data);

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        //execute post
        $requestResponse = array(
            "response" => curl_exec($ch),
            "status" => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

        curl_close($ch);

        return $requestResponse;
    }

    public static function getPostRequestStatus($postRequestResponse)
    {
        return $postRequestResponse["status"];
    }

    public static function executeShellCmd(string $cmd, array $options = array())
    {
        $cmdStart = self::sanitizePath($cmd);

        foreach ($options as $optionName => $optionValue) {
            $cmdStart .= " --" . $optionName . " " . $optionValue;
        }

        return shell_exec($cmdStart);
    }

    public static function sanitizePath(string $path)
    {
        return str_replace("/", DIRECTORY_SEPARATOR, $path);
    }
}
