Feature: isUser cmd
    Test api cmd isUser


    Scenario: Call isUser with an invalid user
        Given the request body is :
            """
            {
                "header": {
                    "private_key": "96436d48-79150c9f-reb3720a-1d352ea9"
                },
                "request": {
                    "email": "UNKNOW@USER.com",
                    "password": "test"
                }
            }
            """
        And I call "isUser" cmd from API
        Then I should get a response with success "true"
