Feature: Common tests for all cmds

    Scenario Outline: Test cmds reachability
        Given the request body is :
            """
            {}
            """
        And I call <cmd> cmd from API
        Then I should get a status "200"
        And I should get a response with success "false"
        Examples:
            | cmd                              |
            | "changePwd"                      |
            #| "createUser"                     |
            | "createUserAwaitingConfirmation" |
            | "getDayGiftData"                 |
            | "getUserData"                    |
            | "isTokenValid"                   |
            | "isUser"                         |
            #| "resetPwd"                       |
            | "sendEmailToAdmin"               |
            | "sendForgetPwdMail"              |
            | "setLang"                        |